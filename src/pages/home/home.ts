import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private afAuth: AngularFireAuth, private toastcontroller: ToastController){
  }

  ionViewWillLoad(){
    this.afAuth.authState.subscribe(data => { 
      if(data.email && data.uid){
        this.toastcontroller.create({
          message: `Bienvenido a APP_NAME, ${data.email}`,
          duration: 3000
        }).present();
      }else{
        this.toastcontroller.create({
          message: `No Pudo Inciar sesión`,
          duration: 3000
        }).present();
      }     
    })
  }
}
